/**
 * Sultans of Swing.
 * make some animation with meaningless calculation.
 * 
 * Processing 3.5.3
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2020.06.27
 */

Calculator cc;

int   frmMax   = 24 * 6;
int   ptnMax   = 3;

int   initPlot = 0;
float initHue  = 0.0;
float initRad  = 0.0;

public void setup() {
  size(720, 720, P2D);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
}

public void draw() {

  int frmCnt = frameCount - 1; // because of frameCount start from 1
  if (frmCnt >= frmMax * ptnMax - 1) {
    exit();
  }

  // reset the animation pattern
  if (frmCnt % frmMax == 0) {
    resetPtn();
  }

  // moving factor
  float frmRatio = easeInOutCosine(map(frmCnt % frmMax, 0, frmMax, 0.0, 1.0));
  float radVal = initRad + frmRatio * TWO_PI * 2.0;

  pushMatrix();
  translate(width * 0.5, height * 0.5);
  rotate(initRad * 0.25);
  background(0.0, 0.0, 90.0, 100.0);
  noFill();
  strokeWeight(1.5);

  beginShape();
  for (int i = 0; i < initPlot; i++) {
		float plotRatio = map(i, 0, initPlot, 0.0, 1.0);
    // meaningless wave formula
		float fX = plotRatio * width;
		float fY = cc.calc(plotRatio, radVal) * height;
    // wave shape in normal coordinate
    float nX = (fX - width  * 0.5);
    float nY = (fY - height * 0.5);
    // wave shape in polar coordinate
    float pX = fY * cos(fX) * 0.5;
    float pY = fY * sin(fX) * 0.5;
    // morphing
		float x = nX * (1.0 - frmRatio) + pX * frmRatio;
		float y = nY * (1.0 - frmRatio) + pY * frmRatio;

    stroke(
           (initHue + plotRatio * 60.0)% 360.0,
           map(plotRatio, 0.0, 1.0, 90.0, 60.0),
           map(plotRatio, 0.0, 1.0, 40.0, 80.0),
           100.0
           );
    vertex(x, y);
	}

  endShape();
  popMatrix();

  casing();
  saveFrame("frames/" + String.format("%04d", frmCnt) + "00.png");

  // for stop motion on ending of each animation pattern
  if (frmCnt % frmMax == frmMax - 1) {
    for (int i = 0; i < 6; i++) {
      saveFrame("frames/" + String.format("%04d", frmCnt) + String.format("%02d", i) + ".png");
    }
  }
  
}

/**
 * resetPtn reset pattern parameters (global variables).
 */
public void resetPtn() {
  cc = getCalculator();
  initPlot = floor(random(80.0, 230.0));
  initHue  = random(360.0);
  initRad  = floor(random(-2.0, 3.0)) * TWO_PI; // for nice size in the last
}

/**
 * getCalculator returns random calculator.
 */
public Calculator getCalculator() {
  int calcNum = 6;
  float r = random(1.0);

  if (r < 1.0 / calcNum) {
    return new MagicWand();
  } else if (r < 2.0 / calcNum) {
    return new Mountains();
  } else if (r < 3.0 / calcNum) {
    return new WindBlow();
  } else if (r < 4.0 / calcNum) {
    return new SeeSaw();
  } else if (r < 5.0 / calcNum) {
    return new ThreeStars();
  } else if (r < 6.0 / calcNum) {
    return new BigWave();
  }
  // fail safe
  return new MagicWand();
}

/**
 * casing : draw fancy casing
 */
public void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(30.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(28.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
}

/**
 * easeInOutCosine easing function.
 * @param  _t    0.0 - 1.0 : linear value.
 * @return float 0.0 - 1.0 : eased value.
 */
public float easeInOutCosine(float _t) {
  return 0.5 - cos(PI * _t) * 0.5;
}

/**
 * Calculator calculate with meaningless formula.
 * @param  _a    : any value.
 * @param  _b    : any value.
 * @return float : calculated value.
 */
interface Calculator {
  float calc(float _a, float _b);
}

class MagicWand implements Calculator {
  float calc(float _a, float _b) {
    return _a * (sin(_b) + cos(_b));
  }
}

class Mountains implements Calculator {
  float calc(float _a, float _b) {
    return cos(_a * _b) * sin(_a + _b * 0.5);
  }
}

class WindBlow implements Calculator {
  float calc(float _a, float _b) {
    return pow(sin(_a + _b), 1) * cos(pow(_a, 1));
  }
}

class SeeSaw implements Calculator {
  float calc(float _a, float _b) {
    return cos(_b) * cos(_a * PI); // seesaw
  }
}

class ThreeStars implements Calculator {
  float calc(float _a, float _b) {
    return sin(_a * TWO_PI + _b);
  }
}

class BigWave implements Calculator {
  float calc(float _a, float _b) {
    return sin(_a * TWO_PI + _b) * (cos(_b) + cos(_a * PI));
  }
}


