* Sultans of Swing.
One of the creative coding works by deconbatch.
[[https://www.deconbatch.com/2020/07/sultans-of-swing.html]['Sultans of Swing.' on Deconbatch's Creative Coding : Examples with Code.]]

[[./example01.png][An example image of some shape calculated in polar coordinates.]]

** Description of this creative coding.
   - It's a creative coding animation made with Processing. I altred [[https://www.deconbatch.com/2020/06/twist-in-dark.html][Twist in the Dark.]].
   - This code draws wave shape with some meaningless wave formula and morphs to the shapes that are calculated in polar coordinates.
   - I calculate some wave shape with cc.calc() (meaningless formula).
     : // meaningless wave formula
     : float fX = plotRatio * width;
     : float fY = cc.calc(plotRatio, radVal) * height;
   - And I translate that shape into a normal coordinate and polar coordinate.
     : // wave shape in normal coordinate
     : float nX = (fX - width  * 0.5);
     : float nY = (fY - height * 0.5);
     : // wave shape in polar coordinate
     : float pX = fY * cos(fX) * 0.5;
     : float pY = fY * sin(fX) * 0.5;
     - [[./example11.png][in normal coordinate]]
     - [[./example12.png][in polar coordinate]]
   - Then, I morph these two shapes with this code.
     : // morphing
     : float x = nX * (1.0 - frmRatio) + pX * frmRatio;
     : float y = nY * (1.0 - frmRatio) + pY * frmRatio;
** Change log.
   - created : 2020/07/18
